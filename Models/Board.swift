//
//  Board.swift
//  Deck
//
//  Created by Eugenio Tampieri on 27/12/20.
//  Copyright © 2020 Eugenio Tampieri. All rights reserved.
//

import Foundation
import UIKit

public struct Board: Codable, ParsableHexColour {
    var title: String
    var owner: User
    var color: String
    var archived: Bool
    var labels: [Label]
    #warning("Use appropriate type")
    var acl: [String]
    var permissions: [String:Bool]
    var users: [User]
    var shared: Int
    var stacks: [Stack]
    var deletedAt: Date
    var lastModified: Date
    var id: Int
}
