//
//  Client.swift
//  Deck
//
//  Created by Eugenio Tampieri on 28/12/20.
//  Copyright © 2020 Eugenio Tampieri. All rights reserved.
//

import Foundation

enum HTTPMethod : CustomStringConvertible {
    var description: String {
        switch self {
        case .DELETE: return "DELETE"
        case .GET: return "GET"
        case .POST: return "POST"
        case .PUT: return "PUT"
        }
    }
    
    case GET
    case POST
    case PUT
    case DELETE
}

class Client {
    private var root: URL
    private var user: String
    private var password: String
    private let decoder: JSONDecoder
    
    init(root: String, user: String, password: String) {
        self.root = URL(string: root + "/index.php/apps/deck/api/v1.0")!
        self.user = user
        self.password = password
        self.decoder = JSONDecoder()
        self.decoder.dateDecodingStrategy = .secondsSince1970
    }
    
    private func makeRequest(endpoint: String, data: String?, method: HTTPMethod, completion: @escaping (Data) -> Void) {
        let requestURL = self.root.appendingPathComponent(endpoint)
        var components = URLComponents(url: requestURL, resolvingAgainstBaseURL: true)!
        if let query = data, method == .GET {
            components.query = query
        }
        
        var request = URLRequest(url: components.url!)
        request.httpMethod = method.description
        request.setValue("Basic \(Data("\(self.user):\(self.password)".utf8).base64EncodedString())", forHTTPHeaderField: "Authorization")
        request.setValue("true", forHTTPHeaderField: "OCS-APIRequest")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        if method != .GET {
            request.httpBody = data?.data(using: .utf8)
        }
        
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let _ = error {
                return
            }
            guard let httpResponse = response as? HTTPURLResponse,
                        (200...299).contains(httpResponse.statusCode) else {
                        return
                    }
            if let mimeType = httpResponse.mimeType, mimeType == "application/json"{
                completion(data!)
                }
            }
            
        task.resume()
    }
    
    func getBoards(onCompletion: @escaping ([Board]?) -> Void) {
        self.makeRequest(endpoint: "/boards", data: "details=true", method: .GET, completion: {data in
            onCompletion(try! self.decoder.decode([Board].self, from: data))
        })
    }
    
    func getCardsFor(board: Int, stack: Int, onCompletion: @escaping ([Card]?) -> Void) {
        self.makeRequest(endpoint: "/boards/\(board)/stacks/\(stack)", data: nil, method: .GET, completion: {data in
            struct Response: Codable {
                var cards: [Card]
            }
            onCompletion(try! self.decoder.decode(Response.self, from: data).cards)
        })

    }
}
