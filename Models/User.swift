//
//  User.swift
//  Deck
//
//  Created by Eugenio Tampieri on 27/12/20.
//  Copyright © 2020 Eugenio Tampieri. All rights reserved.
//

import Foundation

public struct User: Codable {
    var primaryKey: String
    var uid: String
    var displayname: String
    var type: Int
}
