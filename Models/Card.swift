//
//  Card.swift
//  Deck
//
//  Created by Eugenio Tampieri on 28/12/20.
//  Copyright © 2020 Eugenio Tampieri. All rights reserved.
//

import Foundation

struct Card: Codable {
    struct AssignedUser: Codable {
        var id: Int
        var participant: User
        var cardId: Int
        var type: Int
    }
    var title: String
    var description: String
    var stackId: Int
    var type: String
    var lastModified: Date
    var lastEditor: User?
    var createdAt: Date
    var labels: [Label]?
    var assignedUsers: [AssignedUser]
    #warning("Fix type")
    var attachments: [String]?
    var attachmentCount: Int
    var owner: String
    var order: Int
    var archived: Bool
    private var duedate: String?
    var dueDate: Date? {
        get {
            let df = DateFormatter()
            df.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
            return df.date(from: duedate ?? "")
        }
    }
    var deletedAt: Date
    var commentsUnread: Int
    var id: Int
    var overdue: Int
}
