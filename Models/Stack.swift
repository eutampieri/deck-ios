//
//  Stack.swift
//  Deck
//
//  Created by Eugenio Tampieri on 28/12/20.
//  Copyright © 2020 Eugenio Tampieri. All rights reserved.
//

import Foundation

class Stack: Codable {
    var title: String
    var boardId: Int
    var deletedAt: Date
    var lastModified: Date
    var order: Int
    var id: Int
    public func getCards(using client: Client, onCompletion: @escaping ([Card]?) -> Void) {
        client.getCardsFor(board: self.boardId, stack: self.id, onCompletion: onCompletion)
    }
}
