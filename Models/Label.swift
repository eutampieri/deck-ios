//
//  Labels.swift
//  Deck
//
//  Created by Eugenio Tampieri on 28/12/20.
//  Copyright © 2020 Eugenio Tampieri. All rights reserved.
//

import Foundation

struct Label: Codable, ParsableHexColour {
    var title: String
    var color: String
    var boardId: Int
    var cardId: Int?
    var lastModified: Date
    var id: Int
}
