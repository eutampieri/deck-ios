//
//  BoardCollectionViewController.swift
//  Deck
//
//  Created by Eugenio Tampieri on 29/12/20.
//  Copyright © 2020 Eugenio Tampieri. All rights reserved.
//

import Foundation
import UIKit

class BoardCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var stacks: [Stack]?
    var client = Client(root: "https://cloud.bacchilegaeditore.it", user: "eutampieri", password: "939-ypR-cyy-7mu")
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.alwaysBounceVertical = true
        self.collectionView.addSubview(refreshControl)
        self.refreshControl.addTarget(self, action: #selector(self.loadData), for: .valueChanged)
        loadData()
        setupAddButtonItem()
        updateCollectionViewItem(with: view.bounds.size)
    }
    
    @objc func loadData() {
        client.getBoards(onCompletion: {boards in
            self.stacks = boards![0].stacks
            DispatchQueue.main.async {
                self.collectionView.reloadData()
                self.refreshControl.endRefreshing()
            }
        })
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        updateCollectionViewItem(with: size)
    }
  
    private func updateCollectionViewItem(with size: CGSize) {
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        layout.itemSize = CGSize(width: 225, height: size.height * 0.8)
    }
    
    func setupAddButtonItem() {
        let addButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addListTapped(_:)))
        navigationItem.rightBarButtonItem = addButtonItem
    }

    @objc func addListTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Add List", message: nil, preferredStyle: .alert)
        alertController.addTextField(configurationHandler: nil)
        alertController.addAction(UIAlertAction(title: "Add", style: .default, handler: { (_) in
            guard let text = alertController.textFields?.first?.text, !text.isEmpty else {
                return
            }
            
            //self.boards.append(Board(title: text, items: []))
            
            let addedIndexPath = IndexPath(item: self.stacks?.count ?? 0 - 1, section: 0)
            
            self.collectionView.insertItems(at: [addedIndexPath])
            self.collectionView.scrollToItem(at: addedIndexPath, at: UICollectionView.ScrollPosition.centeredHorizontally, animated: true)
        }))
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(alertController, animated: true)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return stacks?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! BoardCollectionViewCell
        
        if let stacks = stacks {
            cell.setup(with: stacks[indexPath.item], client: self.client)
        }
        //cell.layer.borderColor = UIColor(
        cell.layer.borderWidth = CGFloat(2.0)
        return cell
    }
    
}
